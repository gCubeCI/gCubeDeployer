# Pipeline-gCubeDeployer

Jenkins Pipeline script to manage one or more gCube components deployment

## Requirements
* [Jenkins](https://jenkins.io/) ver. 2.164.2 or newer
* [Pipeline plugin](https://wiki.jenkins.io/display/JENKINS/Pipeline+Plugin)
* [Pipeline: Maven](https://plugins.jenkins.io/pipeline-maven)
* [Pipeline: Basic Steps](https://plugins.jenkins.io/workflow-basic-steps) 
* [Email Extension](https://plugins.jenkins.io/email-ext) (to send emails with attachments)
* Jenkins agent(s) configured with Ansible ver. 2.9.27 or newer
* One or more Jenkins agents labeled as 'ansible'

## Expected usage
* ContinuousDeployment(CD): configured as post step build on jenkinsjob
* Scheduled by system: In this case all the pending deployments (CD) will be managed by a temporized trigger 
* Manually from Jenkins GUI

## Expected environment variables
The variables below must be specified as environment variables on the agent where the deployments will be executed:
* IS_SCHEDULED (True | False): if true all the deployment requests will be performed periodically as specified by cron
* ENVIRONMENT: ( DEV | PREPROD | PROD) Indicates the target environment where will be deployed all the artifacts
* DEPLOY_CATEGORY: (Related to release build) one or more Component group (separated by commas) on yaml file where search the components to deploy
* CD: (Continuous Deployment: True | False) if True the remote deploy will be performed

## References 
* [Jenkins Pipeline](https://jenkins.io/doc/book/pipeline/) 
* [Pipeline Syntax](https://jenkins.io/doc/book/pipeline/syntax/)

## Examples 
* When configured as post step build on jenkinsjob, there are some variables to pass as triggered parameters. All the values are dinamically set as shown below:

[[images/gCubeDeployer-JenkinsJob.png]]

* The pipeline can run manually. This option is conceived for debug purpose:

[[images/gCubeDeployer-manualRun.png]]

* Follows a stage view example when the pipeline was scheduled by system, but there were any action to do:

[[images/gCubeDeployer-stageview.png]]



## Flow Chart

[[images/FlowChartgCubeDeployer.jpeg]]


## License
This project is licensed under the [EUPL V.1.1 License](LICENSE.md).
